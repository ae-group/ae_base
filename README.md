<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.95 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# base 0.3.50

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_base/develop?logo=python)](
    https://gitlab.com/ae-group/ae_base)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_base/release0.3.49?logo=python)](
    https://gitlab.com/ae-group/ae_base/-/tree/release0.3.49)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_base)](
    https://pypi.org/project/ae-base/#history)

>ae_base module 0.3.50.

[![Coverage](https://ae-group.gitlab.io/ae_base/coverage.svg)](
    https://ae-group.gitlab.io/ae_base/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_base/mypy.svg)](
    https://ae-group.gitlab.io/ae_base/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_base/pylint.svg)](
    https://ae-group.gitlab.io/ae_base/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_base)](
    https://gitlab.com/ae-group/ae_base/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_base)](
    https://gitlab.com/ae-group/ae_base/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_base)](
    https://gitlab.com/ae-group/ae_base/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_base)](
    https://pypi.org/project/ae-base/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_base)](
    https://gitlab.com/ae-group/ae_base/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_base)](
    https://libraries.io/pypi/ae-base)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_base)](
    https://pypi.org/project/ae-base/#files)


## installation


execute the following command to install the
ae.base module
in the currently active virtual environment:
 
```shell script
pip install ae-base
```

if you want to contribute to this portion then first fork
[the ae_base repository at GitLab](
https://gitlab.com/ae-group/ae_base "ae.base code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_base):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_base/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.base.html
"ae_base documentation").
